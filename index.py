from modules import top_posts
from modules import users
from modules import posts
from modules import topics
from modules import search

# [topics/search] -> posts -> comments -> users

results = top_posts.get_results()

politics = topics.get_topic("politics")

search_results = search.get_search("leaked database")

user = users.get_user("powerfulguy")

post = user["latest_posts"][0]

post_info = posts.get_post(post["link"])

print("Top Posts")

for top_post in results[:10]: #top 10 posts
    print("\t", top_post["text"], top_post["link"])

print()

print("Politics posts")

# print(politics)

print(search_results)

print("User info for 'powerfulguy'")

print("\tName:", user["name"])
print("\tJoined:", user["joined_at"])
print("\tLast Seen:", user["last_seen"])

print()

print("Latest post for 'powerfulguy'")

print("\tPost Title:", post["name"])
print("\tPost Topic:", post["topic"]["name"])
print("\tPost Content:", post_info["text"])
print()
print("\tPost Comments")

for comment in post_info["comments"]:
    print("\t\tComment By:", comment["user"])
    if hasattr(comment, "reply_to"):
        print("\t\t\tReply To:", comment["reply_to"])
        print("\t\t\tWho Said:", comment["reply_to"]["text"])
        print()
    print("\t\tText:", comment["text"])
    print()