import requests
import bs4

def _get_user_soup(user):
    html = requests.get("https://www.nairaland.com/" + user).text
    soup = bs4.BeautifulSoup(html, "html.parser")
    return soup


def get_user(user):
    soup = _get_user_soup(user)
    latest_posts_tags = soup.select("td[id]")
    info_tags = soup.select("table")[2].find_all("b")[-4:]

    user_obj = {}

    user_obj["name"] = user
    user_obj["joined_at"] = info_tags[0].next_sibling[2:]
    user_obj["time_spent_on_site"] = info_tags[1].next_sibling[2:]
    user_obj["last_seen"] = info_tags[3].text
    user_obj["latest_posts"] = []

    for latest_post_tag in latest_posts_tags:
        (topic, post_link) = latest_post_tag.find_all("a")[1:3]
        post = {
            "topic": {
                "name": topic.text,
                "link": topic["href"]
            },
            "link": post_link["href"],
            "name": post_link.text
        }

        user_obj["latest_posts"].append(post)

    return user_obj

def print_user(user):
    
    user_obj = get_user(user)

    print(user_obj)



if __name__ == "__main__":
    print_user("powerfulguy")