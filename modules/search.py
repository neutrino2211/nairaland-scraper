import requests
import bs4

def _get_search_soup(search):
    html = requests.get("https://www.nairaland.com/" + search).text
    soup = bs4.BeautifulSoup(html, "html.parser")
    return soup


def get_search(search):

    query_string = search.replace(" ", "+")
    search = f"/search?q={query_string}&board=0"

    soup = _get_search_soup(search)

    search_list = []

    divs = soup.select('table')[2].find_all("div")

    for div in divs:
        search_header = div.parent.parent.previous_sibling.parent
        links = search_header.select('a[href]')
        topic = links[0]
        post_link = links[1]
        user = links[2]

        post_obj = {}
        post_obj["user"] = user["href"][1:]
        post_obj["text"] = ""
        post_obj["topic"] = topic["href"]
        post_obj["link"] = post_link["href"]
        post_obj["is_comment"] = post_link.text.startswith("Re:")

        if post_obj["is_comment"]:
            post_obj["comment_topic"] = "/".join(post_link["href"].split("/")[:3])
        
        
        if div.blockquote:
            for child in div.children:
                if child.name == "blockquote":
                    text = child.text.split(":")
                    if not hasattr(post_obj, "reply_to"):
                        post_obj["reply_to"] = {
                            "user": text[0],
                            "text": ':'.join(text[1:])
                        }
                    else:
                        post_obj["reply_to"]["text"] += ':'.join(text)
                elif child.name == None:
                    post_obj["text"] += child
        else:
            post_obj["text"] = div.text
        
        search_list.append(post_obj)

    return search_list

def print_search(search):
    
    search_obj = get_search(search)
    for search in search_obj:
        print(search)
        print()



if __name__ == "__main__":
    print_search("chelsea")