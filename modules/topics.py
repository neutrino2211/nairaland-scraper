from bs4 import BeautifulSoup
import requests

def _get_topic_soup(topic):
    html_text = requests.get('https://www.nairaland.com/' + topic).text
    soup = BeautifulSoup(html_text, 'html.parser')
    return soup

def get_topic(topic):
    soup = _get_topic_soup(topic)
    news = soup.select('td[id]')

    topic_obj = {}

    topic_obj["name"] = topic
    topic_obj["posts"] = []

    for news_item in news:
        topic_post_obj = {}
        trend = news_item.find('b').find('a')

        topic_post_obj["link"] = trend['href']
        topic_post_obj["text"] = trend.text

        topic_obj["posts"].append(topic_post_obj)
        
    return topic_obj

def print_topic(topic):

    topic_obj = get_topic(topic)
    print(topic_obj)