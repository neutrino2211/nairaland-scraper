import requests
import bs4

def _get_post_soup(post):
    html = requests.get("https://www.nairaland.com/" + post).text
    soup = bs4.BeautifulSoup(html, "html.parser")
    return soup


def get_post(post):
    soup = _get_post_soup(post)

    post_obj = {}

    divs = soup.select('table[summary="posts"]')[0].find_all("div")
    # print([0])

    post_obj["text"] = divs[0].blockquote.text
    post_obj["comments"] = []

    post_obj["user"] = divs[0].parent.parent.previous_sibling.parent.select('a[href]')[1]["href"][1:]

    for div in divs[1:]:
        post_header = div.parent.parent.previous_sibling.parent
        user = post_header.select('a[href]')[1]

        comment_obj = {}
        comment_obj["user"] = user["href"][1:]
        comment_obj["text"] = ""
        # print(user)
        # print(div.text)
        if div.blockquote:
            for child in div.children:
                if child.name == "blockquote":
                    text = child.text.split(":")
                    comment_obj["reply_to"] = {
                        "user": text[0],
                        "text": text[1]
                    }
                elif child.name == None:
                    comment_obj["text"] += child
        else:
            comment_obj["text"] = div.text
        
        post_obj["comments"].append(comment_obj)

    return post_obj

def print_post(post):
    
    post_obj = get_post(post)

    print(post_obj)



if __name__ == "__main__":
    print_post("/6585442/official-2021-uefa-super-cup")