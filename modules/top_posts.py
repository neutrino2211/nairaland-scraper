import requests
import bs4

html = requests.get("https://www.nairaland.com").text
soup = bs4.BeautifulSoup(html, "html.parser")


def print_results():
    a_tags = soup.select("td.featured > a")

    print("Featured posts")
    for a_tag in a_tags:
        print("\t -> " + a_tag.text, a_tag["href"])

def get_results():
    results = []

    a_tags = soup.select("td.featured > a")

    for a_tag in a_tags:
        results.append({
            "text": a_tag.text if hasattr(a_tag, "text") else "",
            "link": a_tag["href"]
        })

    return results